//
//  ViewController.swift
//  Health For You
//
//  Created by SSCS on 03/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ViewController: UIViewController {

    @IBOutlet weak var txt_email: SkyFloatingLabelTextField!
    @IBOutlet weak var txt_Password: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        self.txt_email.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
         self.txt_Password.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btn_Tapped_Signin(_ sender: Any) {
        self.performSegue(withIdentifier: "segue_Home", sender: self)
    }

}

