//
//  HomeViewController.swift
//  The Mint Club
//
//  Created by SSCS on 16/12/17.
//  Copyright © 2017 SSCS. All rights reserved.
//

import UIKit



private var Category_VC: CategoryViewController = {
    // Load Storyboard
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

    // Instantiate View Controller
    var viewController = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
    return viewController
}()

private var Article_VC: ArticlesViewController = {
    // Load Storyboard
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    var viewController = storyboard.instantiateViewController(withIdentifier: "ArticlesViewController") as! ArticlesViewController
    return viewController
}()

private var EmergancyVC: EmergancyViewController = {
    // Load Storyboard
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    var viewController = storyboard.instantiateViewController(withIdentifier: "EmergancyViewController") as! EmergancyViewController
    return viewController
}()
private var ProfileVC: ProfileViewController = {
    // Load Storyboard
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    var viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
    return viewController
}()

class HomeViewController: UIViewController {

  
    @IBOutlet weak var img_home: UIImageView!
    @IBOutlet weak var img_artciles: UIImageView!
    @IBOutlet weak var img_emergancy: UIImageView!
    @IBOutlet weak var img_profile: UIImageView!
    
    @IBOutlet weak var lbl_home: UILabel!
    @IBOutlet weak var lbl_articles: UILabel!
    @IBOutlet weak var lbl_emergancy: UILabel!
    @IBOutlet weak var lbl_profile: UILabel!
    
    @IBOutlet weak var lbl_center_constaint: NSLayoutConstraint!
 
    @IBOutlet weak var view_Parent: UIView!
    @IBOutlet weak var lbl_nav_title: UILabel!
    @IBOutlet weak var view_home: UIView!
    var is_tapped_segment:Int = 0
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
      //   UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.Set_Navigationbar_Button()
        
        self.displayContentController(content: Category_VC)
        
       // self.Set_SegmentBar()
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReceivedNotification(notification:)), name: Notification.Name(ARTICLES_DETAIL), object: nil)

    }
    
    @objc func ReceivedNotification(notification:NSNotification) {
        
        let userinfo:Dictionary<String,Any> = notification.userInfo as! Dictionary<String, Any>
        
        if (userinfo["Type"] as! String) == "1"
        {
            let vc:Sub_CategoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "Sub_CategoryViewController") as! Sub_CategoryViewController
            vc.str_Nav_title = "Dentist"
            self.navigationController?.pushViewController(vc , animated: true)
        }
        else  if (userinfo["Type"] as! String) == "2"
        {
            let vc:ArticlesDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ArticlesDetailViewController") as! ArticlesDetailViewController
            self.navigationController?.pushViewController(vc , animated: true)
        }
        else if (userinfo["Type"] as! String) == "3"
        {
            let vc:EmergancyCategoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "EmergancyCategoryViewController") as! EmergancyCategoryViewController
            self.navigationController?.pushViewController(vc , animated: true)
        }
        else{
            
        }
      
        
    }
    
    func Set_Navigationbar_Button() {
        
        let btnLogo = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnLogo.setImage(UIImage(named: "ic_sidemenu"), for: .normal)
        btnLogo.addTarget(self, action: #selector(self.Tapped_SideMenu), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLogo)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func Tapped_SideMenu(){
        self.slideMenuController()?.toggleLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Health For u"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if is_tapped_segment == 0
        {
        }
    }
    
    @IBAction func btn_Tapped_Segment(_ sender: UIButton) {
        is_tapped_segment = 1
       
        switch sender.tag {
        case 101:
            
            self.lbl_home.textColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
            self.lbl_articles.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_emergancy.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_profile.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            
            self.img_home.image = UIImage(named: "ic_sel_home")
            self.img_artciles.image = UIImage(named: "ic_unsel_article")
            self.img_emergancy.image = UIImage(named: "ic_unsel_emergancy")
            self.img_profile.image = UIImage(named: "ic_unsel_profile")
           
            self.displayContentController(content: Category_VC)
            break
        case 102:
            
            
            self.lbl_articles.textColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
            self.lbl_home.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_emergancy.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_profile.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            
            self.img_home.image = UIImage(named: "ic_unsele_home")
            self.img_artciles.image = UIImage(named: "ic_sel_article")
            self.img_emergancy.image = UIImage(named: "ic_unsel_emergancy")
            self.img_profile.image = UIImage(named: "ic_unsel_profile")
            
            self.displayContentController(content: Article_VC)

            break
        case 103:
            
            self.lbl_emergancy.textColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
            self.lbl_home.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_articles.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_profile.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            
            self.img_home.image = UIImage(named: "ic_unsele_home")
            self.img_artciles.image = UIImage(named: "ic_unsel_article")
            self.img_emergancy.image = UIImage(named: "ic_sel_emergancy")
            self.img_profile.image = UIImage(named: "ic_unsel_profile")
            
         
            self.displayContentController(content: EmergancyVC)

            break
        case 104:
            
            self.lbl_profile.textColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
            self.lbl_home.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_emergancy.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            self.lbl_articles.textColor = UIColor(red: 200.0 / 255.0, green: 200.0 / 255.0, blue: 200.0 / 255.0, alpha: 1)
            
            self.img_home.image = UIImage(named: "ic_unsele_home")
            self.img_artciles.image = UIImage(named: "ic_unsel_article")
            self.img_emergancy.image = UIImage(named: "ic_unsel_emergancy")
            self.img_profile.image = UIImage(named: "ic_sel_profile")
           
            
            self.displayContentController(content: ProfileVC)
            
            break
            
        default:
            print("temp")
            break
        }
        
        
    }
    
    @IBAction func btn_Tapped_menu(_ sender: UIButton) {
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func btn_Tapped_segment(_ sender: UIButton) {
        
//        is_tapped_segment = 1
//        var width:CGFloat = 0.0
//        switch sender.tag {
//        case 101:
//
//            self.lbl_nav_title.text = "The Mint Club"
//            self.img_home.image = UIImage(named: "ic_select_home")
//            self.img_event.image = UIImage(named: "ic_unselect_event")
//            self.img_activity.image = UIImage(named: "ic_unselect_activity")
//            self.img_gallary.image = UIImage(named: "ic_unselect_gallary")
//            width = 0.0
//            self.displayContentController(content: CompleteEventViewController)
//            self.Addanimation_addchildvc(vc: CompleteEventViewController)
//            break
//        case 102:
//
//            self.lbl_nav_title.text = "Events"
//            self.img_home.image = UIImage(named: "ic_unselect_home")
//            self.img_event.image = UIImage(named: "ic_select_event")
//            self.img_activity.image = UIImage(named: "ic_unselect_activity")
//            self.img_gallary.image = UIImage(named: "ic_unselect_gallary")
//
//            width = CGFloat(self.view_home.frame.size.width)
//            self.displayContentController(content: UpcommingEventVC)
//            self.Addanimation_addchildvc(vc: UpcommingEventVC)
//
//            break
//        case 103:
//
//            self.lbl_nav_title.text = "Facilities"
//
//            self.img_home.image = UIImage(named: "ic_unselect_home")
//            self.img_event.image = UIImage(named: "ic_unselect_event")
//            self.img_activity.image = UIImage(named: "ic_select_activity")
//            self.img_gallary.image = UIImage(named: "ic_unselect_gallary")
//             width = CGFloat(self.view_home.frame.size.width)*2
//            self.displayContentController(content: FacilityVC)
//             self.Addanimation_addchildvc(vc: FacilityVC)
//            //self.view_home.frame.size.width*2
//            break
//        case 104:
//
//            self.lbl_nav_title.text = "Gallary"
//            self.img_home.image = UIImage(named: "ic_unselect_home")
//            self.img_event.image = UIImage(named: "ic_unselect_event")
//            self.img_activity.image = UIImage(named: "ic_unselect_activity")
//            self.img_gallary.image = UIImage(named: "ic_select_gallary")
//             width = CGFloat(self.view_home.frame.size.width)*3
//
//            self.displayContentController(content: GallaryVC)
//
//            break
//
//        default:
//            print("temp")
//            break
//        }
//
//        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
//
//            self.lbl_center_constaint.constant = width
//            self.view.layoutIfNeeded()
//
//        } ) { (completed) in
//
//        }
    }
    
    func Addanimation_addchildvc(vc:UIViewController) {

        vc.view.alpha = 0
        vc.view.layoutIfNeeded()
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .transitionFlipFromRight , animations: {
             vc.view.alpha = 1
            
        }) { (completed) in
            
        }
    }
    
    func displayContentController(content: UIViewController) {
        
        for view in self.view_Parent.subviews {
            view.removeFromSuperview()
        }
        content.view.frame = CGRect(x: 0, y: 0, width: self.view_Parent.frame.size.width, height:self.view_Parent.frame.size.height )
        addChildViewController(content)
        self.view_Parent.addSubview(content.view)
        content.didMove(toParentViewController: self)
    }
    
    func hideContentController(content: UIViewController) {
        content.willMove(toParentViewController: nil)
        content.view.removeFromSuperview()
        content.removeFromParentViewController()
    }
    
}

