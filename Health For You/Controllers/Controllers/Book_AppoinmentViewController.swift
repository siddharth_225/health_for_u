//
//  Book_AppoinmentViewController.swift
//  Health For You
//
//  Created by SSCS on 08/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import WWCalendarTimeSelector

class Book_AppoinmentViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , WWCalendarTimeSelectorProtocol{

    @IBOutlet weak var lbl_bg: UILabel!
    @IBOutlet weak var img_post: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_fees: UILabel!
    
    @IBOutlet weak var img_star1: UIImageView!
    @IBOutlet weak var img_star2: UIImageView!
    @IBOutlet weak var img_star3: UIImageView!
    @IBOutlet weak var img_star4: UIImageView!
    @IBOutlet weak var img_star5: UIImageView!
    
    @IBOutlet weak var lbl_availibility: UILabel!
    
    @IBOutlet weak var btn_date: UIButton!
    @IBOutlet weak var txt_reason_visit: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lbl_time: UILabel!
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    var arr_time:[String] = ["9:00 AM","10:00 AM","11:00 AM","12:00 PM","4:00 PM","5:00 PM","6:00 PM","7:00 PM"]
    
    let Blue_Color:UIColor = UIColor(red: 17.0 / 255.0, green: 48.0 / 255.0, blue: 96.0 / 255.0, alpha: 1)
    
    var dateToSelect: Date!
    @IBOutlet weak var collectionview_appoinment: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.set_Layout()
        dateToSelect = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM,yyyy"
        self.btn_date.setTitle(formatter.string(from:Date()), for: .normal)
        
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationItem.title = "BOOK YOUR APPOINMENT"
        self.txt_reason_visit.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
      //  self.txt_reason_visit.placeholderFont = UIFont.systemFont(ofSize: 10)
        // Do any additional setup after loading the view.
    }
    
    func set_Layout() {
        self.img_post.layer.cornerRadius = self.img_post.frame.size.height/2
        self.img_post.layer.masksToBounds = true
        lbl_bg.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 2, scale: true)
        
        let btnLogo = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnLogo.setImage(UIImage(named: "ic_unfav"), for: .normal)
        btnLogo.setImage(UIImage(named: "ic_fav"), for: .selected)
        btnLogo.isSelected = false
        btnLogo.addTarget(self, action: #selector(self.Tapped_Favourite), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLogo)
        self.navigationItem.rightBarButtonItem = barButton
        
        let btnback = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnback.setImage(UIImage(named: "ic_back"), for: .normal)
        btnback.addTarget(self, action: #selector(self.Tapped_Back), for: .touchUpInside)
        let barButton1 = UIBarButtonItem(customView: btnback)
        self.navigationItem.leftBarButtonItem = barButton1
    }
    
    @objc func Tapped_Back() {
        _=self.navigationController?.popViewController(animated: true)
    }
    @objc func Tapped_Favourite(button:UIButton) {
        
        if button.isSelected
        {
            button.isSelected = false
        }else{
            button.isSelected = true
        }
    }
    
    // MARK: - Get Next and Previous Date Method
    func Get_Next_Date() -> Date {
        print(Calendar.current.date(byAdding: .day, value: 1, to: dateToSelect)!)
        dateToSelect = Calendar.current.date(byAdding: .day, value: 1, to: dateToSelect)!
        return  dateToSelect
    }
    
    func Get_Previous_Date() -> Date {
        print(Calendar.current.date(byAdding: .day, value: -1, to: dateToSelect)!)
        dateToSelect = Calendar.current.date(byAdding: .day, value: -1, to: dateToSelect)!
        return  dateToSelect
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_Tapped_Next_Date(_ sender: Any) {
        let formatter = DateFormatter()
       
        // initially set the format based on your datepicker date
        formatter.dateFormat = "dd MMM,yyyy"
        self.btn_date.setTitle(formatter.string(from: self.Get_Next_Date()), for: .normal)
    }
    
    @IBAction func btn_Tapped_Previous_Date(_ sender: Any) {
        let formatter = DateFormatter()
        
        // initially set the format based on your datepicker date
        formatter.dateFormat = "dd MMM,yyyy"
        self.btn_date.setTitle(formatter.string(from: self.Get_Previous_Date()), for: .normal)
    }
    
    @IBAction func btn_Tapped_Current_date(_ sender: Any) {
        
        
        let selector = WWCalendarTimeSelector.instantiate()
        
        // 2. You can then set delegate, and any customization options
        selector.delegate = self
        selector.optionTopPanelTitle = "Select Appoinment Date!"
        selector.optionTopPanelFontColor = Blue_Color
        selector.optionCalendarBackgroundColorTodayFlash = Blue_Color
        selector.optionButtonFontColorDone = Blue_Color
        selector.optionButtonFontColorCancel = Blue_Color
        selector.optionSelectorPanelBackgroundColor = Blue_Color

        selector.optionCalendarBackgroundColorTodayHighlight  = UIColor.red
        selector.optionCalendarBackgroundColorFutureDatesHighlight = Blue_Color
        selector.optionTopPanelFontColor = UIColor.white
        selector.optionTopPanelBackgroundColor = Blue_Color
        selector.optionStyles.showTime(false)
        self.present(selector, animated: true, completion: nil)

    }
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
    
        let formatter = DateFormatter()
        dateToSelect = date
        // initially set the format based on your datepicker date
        formatter.dateFormat = "dd MMM,yyyy"
        self.btn_date.setTitle(formatter.string(from: date), for: .normal)
        
    }
    @IBAction func btn_Tapped_Confirm_Appoinment(_ sender: Any) {
    }
    
    //MARK: Collectionview delegate method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_time.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let lbl_time:UILabel  = cell.contentView.viewWithTag(101) as! UILabel
        lbl_time.text = self.arr_time[indexPath.row]
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
//        cell.layer.cornerRadius = 1
//        cell.layer.borderColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1).cgColor
//        cell.layer.masksToBounds = true
//        cell.backgroundColor = UIColor.white
//        let lbl_time:UILabel  = cell.contentView.viewWithTag(101) as! UILabel
//        lbl_time.textColor = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
//
//    }
//    private func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
//        let cellToDeselect:UICollectionViewCell = collectionView.cellForItem(at: indexPath as IndexPath)!
//        cellToDeselect.contentView.backgroundColor = UIColor.clear
//        let lbl_time:UILabel  = cellToDeselect.contentView.viewWithTag(101) as! UILabel
//        lbl_time.textColor = UIColor.white
//    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
                let cellToDeselect:UICollectionViewCell = collectionView.cellForItem(at: indexPath as IndexPath)!
                cellToDeselect.contentView.backgroundColor = UIColor.clear
                let lbl_time:UILabel  = cellToDeselect.contentView.viewWithTag(101) as! UILabel
                lbl_time.textColor = UIColor.white
    }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedCell:UICollectionViewCell = collectionView.cellForItem(at: indexPath as IndexPath)!
        selectedCell.contentView.backgroundColor = UIColor.white
       
        let lbl_time1:UILabel  = selectedCell.contentView.viewWithTag(101) as! UILabel
        lbl_time1.textColor = UIColor(red: 17.0 / 255.0, green: 48.0 / 255.0, blue: 96.0 / 255.0, alpha: 1)
        lbl_time1.layer.cornerRadius = 1
        lbl_time1.layer.borderWidth = 1
        lbl_time1.layer.borderColor = UIColor(red: 17.0 / 255.0, green: 48.0 / 255.0, blue: 96.0 / 255.0, alpha: 1).cgColor
        lbl_time1.layer.masksToBounds = true
        
        lbl_time.text = self.arr_time[indexPath.row]
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/4, height: 31)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
