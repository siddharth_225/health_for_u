//
//  CategoryViewController.swift
//  Health For You
//
//  Created by SSCS on 05/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {

    @IBOutlet weak var tbl_category: UITableView!
    var arr_name:[String] = ["Doctor","Hospital","Pharmacy","Laboratory","Blood Bank"]
    var arr_img:[String] = ["1.png","2.png","3.png","4.png","5.png"]
    
    var arr_temp:[String] = []
    var tbl_index:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage(named: "")
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
      //  self.tbl_category.reloadData()
       
    }
    //comp: ()-> Void
    
    func Add_tableviewcelll() {
        
        let indexpath = IndexPath(row: tbl_index, section: 0)
        arr_temp.append(arr_name[tbl_index])
        tbl_category.insertRows(at: [indexpath], with:.right)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1){
            self.tbl_index += 1
            self.Add_tableviewcelll()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_img.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        
        let lbl_name = cell.contentView.viewWithTag(102) as! UILabel
        lbl_name.text = self.arr_name[indexPath.row]
        let img_info = cell.contentView.viewWithTag(101) as! UIImageView
        img_info.image = UIImage(named: self.arr_img[indexPath.row])
        //img_info.rotate360Degrees()
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: Notification.Name(ARTICLES_DETAIL), object: nil, userInfo: ["Type":"1"])
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 30, 0)
        cell.layer.transform = transform

        UIView.animate(withDuration: 1) {
               cell.alpha = 1
             cell.layer.transform = CATransform3DIdentity
        }
    }
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   
}

extension UIImageView {
    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI)
        rotateAnimation.duration = duration
        if let delegate: CAAnimationDelegate = completionDelegate as! CAAnimationDelegate? {
            rotateAnimation.delegate = delegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

