//
//  EmergancyViewController.swift
//  Health For You
//
//  Created by SSCS on 05/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class EmergancyViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tbl_emergancy: UITableView!
    
    var arr_images:[String] = ["ic_doctor","ic_hospital","ic_ambulance","ic_pharmacy","ic_bloodbank"]
    var arr_names:[String] = ["DOCTOR","HOSPITAL","AMBULANCE","PHARMACITICS","BLOOD BANK"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_images.count
        //return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell_emer"))!
        
        let lbl_name = cell.contentView.viewWithTag(102) as! UILabel
        let img_info = cell.contentView.viewWithTag(101) as! UIImageView
        let lbl_bg = cell.contentView.viewWithTag(103) as! UILabel
        lbl_bg.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 2, scale: true)
        cell.selectionStyle = .none
        lbl_name.text = self.arr_names[indexPath.row]
        img_info.image = UIImage(named: self.arr_images[indexPath.row])  //UIImageView(named: self.arr_img[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        NotificationCenter.default.post(name: Notification.Name(ARTICLES_DETAIL), object: nil, userInfo: ["Type":"3"])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 30, 0)
        cell.layer.transform = transform
        
        UIView.animate(withDuration: 1) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
