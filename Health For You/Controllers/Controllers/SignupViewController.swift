//
//  SignupViewController.swift
//  Health For You
//
//  Created by SSCS on 04/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SignupViewController: UIViewController {

    
    @IBOutlet weak var txt_lname: SkyFloatingLabelTextField!
    @IBOutlet weak var txt_Fname: SkyFloatingLabelTextField!
    @IBOutlet weak var txt_email: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txt_contact: SkyFloatingLabelTextField!
    @IBOutlet weak var txt_password: SkyFloatingLabelTextField!
    @IBOutlet weak var txt_confirmPassword: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.txt_email.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        self.txt_password.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        self.txt_lname.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        self.txt_Fname.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        self.txt_contact.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        self.txt_confirmPassword.titleFont = UIFont.init(name: "Raleway-Medium", size: 10)!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

  
    @IBAction func btn_Tapped_signup(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
}
