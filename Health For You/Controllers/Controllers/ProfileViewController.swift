//
//  ProfileViewController.swift
//  Health For You
//
//  Created by SSCS on 05/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {


    @IBOutlet weak var lbl_bg: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var img_user_profile: UIImageView!
    
    @IBOutlet weak var tbl_profile: UITableView!
  
    var arr_images:[String] = ["ic_appoinment","ic_payment","ic_star","ic_logout"]
    var arr_names:[String] = ["MY APPOINMENT","MY PAYMENT","FAVOURITE","LOGOUT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_bg.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 2, scale: true)

        img_user_profile.layer.cornerRadius = self.img_user_profile.frame.size.height/2
        img_user_profile.layer.borderWidth = 3
        img_user_profile.layer.borderColor = UIColor(red: (230.0/255.0), green: (230.0/255.0), blue: (230.0/255.0), alpha: 1).cgColor
        img_user_profile.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        
        let lbl_name = cell.contentView.viewWithTag(102) as! UILabel
        let img_info = cell.contentView.viewWithTag(101) as! UIImageView
        let lbl_bg = cell.contentView.viewWithTag(103) as! UILabel
        lbl_bg.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 2, scale: true)
        cell.selectionStyle = .none
        lbl_name.text = self.arr_names[indexPath.row]
        img_info.image = UIImage(named: self.arr_images[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
