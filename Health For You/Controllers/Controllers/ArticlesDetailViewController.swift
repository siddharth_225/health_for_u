//
//  ArticlesDetailViewController.swift
//  Health For You
//
//  Created by SSCS on 06/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class ArticlesDetailViewController: UIViewController {

    @IBOutlet weak var view_height: NSLayoutConstraint!
    @IBOutlet weak var lbl_description: UILabel!
    
    @IBOutlet weak var lbl_sleepLine: UILabel!
    @IBOutlet weak var view_main: UIView!
    
    @IBOutlet weak var img_post: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_likes: UILabel!
    
    @IBOutlet weak var img_like: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationItem.title = "Health For U"

        self.lbl_description.text = "As seen in the last step there are a few different properties needed when adding a shadow using Core Animation. If your are not familiar with Core Animation don’t worry, just try to focus on the shadow code and not the CALayer on the view.\n\n    As seen in the last step there are a few different properties needed when adding a shadow using Core Animation. If your are not familiar with Core Animation don’t worry, just try to focus on the shadow code and not the CALayer on the view.As seen in the last step there are a few different properties needed when adding a shadow using Core Animation. If your are not familiar with Core Animation don’t worry, just try to focus on the shadow code and not the CALayer on the view."
        
        self.view_height.constant = self.lbl_sleepLine.frame.origin.y+10
        
        view_main.layer.masksToBounds = false
        view_main.layer.shadowColor = UIColor.gray.cgColor
        view_main.layer.shadowOpacity = 0.5
        view_main.layer.shadowOffset = CGSize(width:0, height: 0)
        view_main.layer.shadowRadius = 2
        
        let btnback = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnback.setImage(UIImage(named: "ic_back"), for: .normal)
        btnback.addTarget(self, action: #selector(self.Tapped_Back), for: .touchUpInside)
        let barButton1 = UIBarButtonItem(customView: btnback)
        self.navigationItem.leftBarButtonItem = barButton1
        // Do any additional setup after loading the view.
    }

    @objc func Tapped_Back() {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_Tapped_Like(_ sender: Any) {
        
    }
    
    @IBAction func btn_Tapped_Share(_ sender: Any) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
