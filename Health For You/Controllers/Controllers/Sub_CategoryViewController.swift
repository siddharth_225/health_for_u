//
//  Sub_CategoryViewController.swift
//  Health For You
//
//  Created by SSCS on 10/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class Sub_CategoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var tbl_subcategory: UICollectionView!
    
    var arr_name:[String] = ["Dentist","Ayurvedya","Cardiologist","Orthopedist","General Physician","Physiotherapist"]
    var arr_img:[String] = ["6.jpg","7.jpg","8.jpg","9.jpg","10.jpg","11.jpg"]
    var str_Nav_title:String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.navigationController?.navigationItem.hidesBackButton = true
        
        let btnLogo = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        btnLogo.setImage(UIImage(named: "ic_back"), for: .normal)
        btnLogo.imageView?.contentMode = .scaleAspectFit
        btnLogo.addTarget(self, action: #selector(self.Tapped_Back), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLogo)
        self.navigationItem.leftBarButtonItem = barButton
        
        // Do any additional setup after loading the view.
    }
    
    @objc func Tapped_Back() {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        let btnLogo = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        btnLogo.setImage(UIImage(named: "ic_back"), for: .normal)
        btnLogo.addTarget(self, action: #selector(self.Tapped_Back), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLogo)
        
        self.navigationItem.leftBarButtonItem = barButton
     self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationItem.title = str_Nav_title
    }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Collectionview delegate method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_name.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let lbl_time:UILabel  = cell.contentView.viewWithTag(101) as! UILabel
        lbl_time.text = self.arr_name[indexPath.row]
        
        let img_post:UIImageView =  cell.contentView.viewWithTag(102) as! UIImageView
        img_post.image = UIImage(named: self.arr_img[indexPath.row])
       // img_post.rotate360Degrees  ()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cellToDeselect:UICollectionViewCell = collectionView.cellForItem(at: indexPath as IndexPath)!
        cellToDeselect.contentView.backgroundColor = UIColor.clear
        let lbl_time:UILabel  = cellToDeselect.contentView.viewWithTag(101) as! UILabel
        lbl_time.textColor = UIColor.white
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc:EmergancyCategoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "EmergancyCategoryViewController") as! EmergancyCategoryViewController
        self.navigationController?.pushViewController(vc , animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  2
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: 120)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Collectionview_footer", for: indexPath as IndexPath)
       
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForFooterInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width, height: 50)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.bounds.width, height: 50)
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,  referenceSizeForFooterInSection section: Int) -> CGSize
//    {
//        return CGSize(width: collectionView.bounds.width+60, height: 50)
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
