//
//  ArticlesViewController.swift
//  Health For You
//
//  Created by SSCS on 05/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tbl_articles: UITableView!
    
    var arr_name:[String] = ["The table view","Testing purpose","Pharmacy Testing","Laboratory testing","Blood Bank Testing"]
    var arr_img:[String] = ["1.png","2.png","3.png","4.png","5.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr_name.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ArticlesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ArticlesTableViewCell
        cell.lbl_titlle.text = self.arr_name[indexPath.row]
        cell.lbl_description.text = " If you don't pass any parameter to that function, then the scale argument will be true by default. You can define a default value for any parameter in a function by assigning a value to the parameter after that parameter’s type."
        cell.img_post.image = UIImage(named: self.arr_img[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        NotificationCenter.default.post(name: Notification.Name(ARTICLES_DETAIL), object: nil, userInfo: ["Type":"2"])
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 30, 0)
        cell.layer.transform = transform
        
        UIView.animate(withDuration: 1) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
