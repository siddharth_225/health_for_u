//
//  EmergancyCategoryViewController.swift
//  Health For You
//
//  Created by SSCS on 08/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class EmergancyCategoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {
    
    var arr_name:[String] = ["The table view","Testing purpose","Pharmacy Testing","Laboratory testing","Blood Bank Testing"]
    var arr_img:[String] = ["1.png","2.png","3.png","4.png","5.png"]
    
    @IBOutlet weak var tbl_category: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
       
        let btnback = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnback.setImage(UIImage(named: "ic_back"), for: .normal)
        btnback.addTarget(self, action: #selector(self.Tapped_Back), for: .touchUpInside)
        let barButton1 = UIBarButtonItem(customView: btnback)
        self.navigationItem.leftBarButtonItem = barButton1
        // Do any additional setup after loading the view.
    }
    
    @objc func Tapped_Back() {
        _=self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
         self.navigationItem.title = "DOCTOR"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arr_name.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:EmergancyCategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EmergancyCategoryTableViewCell
        cell.lbl_name.text = self.arr_name[indexPath.row].uppercased()
        
        
        let stringValue = "Dentis BDS(7 YEARS Experience)\nManinagar Ahmedabad\n9:00AM TO 6:00 PM"
        let attrString = NSMutableAttributedString(string: stringValue)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 4 // change line spacing between paragraph like 36 or 48
        style.minimumLineHeight = 10 // change line spacing between each line like 30 or 40
        attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: stringValue.characters.count))
        cell.lbl_description.attributedText = attrString
    
        cell.img_post.image = UIImage(named: self.arr_img[indexPath.row])
        
        let index:Int = 3
        
        if index == 1
        {
            cell.img_star1.image = UIImage(named: "ic_fill_star")
            cell.img_star2.image = UIImage(named: "ic_unfill_star")
            cell.img_star3.image = UIImage(named: "ic_unfill_star")
            cell.img_star4.image = UIImage(named: "ic_unfill_star")
            cell.img_star5.image = UIImage(named: "ic_unfill_star")
        }
        else if index == 2
        {
            cell.img_star1.image = UIImage(named: "ic_fill_star")
            cell.img_star2.image = UIImage(named: "ic_fill_star")
            cell.img_star3.image = UIImage(named: "ic_unfill_star")
            cell.img_star4.image = UIImage(named: "ic_unfill_star")
            cell.img_star5.image = UIImage(named: "ic_unfill_star")
        }
        else if index == 3
        {
            cell.img_star1.image = UIImage(named: "ic_fill_star")
            cell.img_star2.image = UIImage(named: "ic_fill_star")
            cell.img_star3.image = UIImage(named: "ic_fill_star")
            cell.img_star4.image = UIImage(named: "ic_unfill_star")
            cell.img_star5.image = UIImage(named: "ic_unfill_star")
        }
        else if index == 4
        {
            cell.img_star1.image = UIImage(named: "ic_fill_star")
            cell.img_star2.image = UIImage(named: "ic_fill_star")
            cell.img_star3.image = UIImage(named: "ic_fill_star")
            cell.img_star4.image = UIImage(named: "ic_fill_star")
            cell.img_star5.image = UIImage(named: "ic_unfill_star")
        }
        else
        {
            cell.img_star1.image = UIImage(named: "ic_fill_star")
            cell.img_star2.image = UIImage(named: "ic_fill_star")
            cell.img_star3.image = UIImage(named: "ic_fill_star")
            cell.img_star4.image = UIImage(named: "ic_fill_star")
            cell.img_star5.image = UIImage(named: "ic_fill_star")
            // break
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc:Book_AppoinmentViewController = self.storyboard?.instantiateViewController(withIdentifier: "Book_AppoinmentViewController") as! Book_AppoinmentViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 30, 0)
        cell.layer.transform = transform
        
        UIView.animate(withDuration: 1) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
