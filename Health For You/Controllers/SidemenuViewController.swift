//
//  SidemenuViewController.swift
//  The Mint Club
//
//  Created by SSCS on 18/12/17.
//  Copyright © 2017 SSCS. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case profile = 0
    //    case logs
    case acceptInventory
    case usedInventory
    case inventoryQueue
    case contactUs
    case logout
}

protocol SideMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}
class SidemenuViewController: UIViewController, SideMenuProtocol,UITableViewDelegate,UITableViewDataSource {
    
    var mainViewController: UITabBarController!
    var profileNavVC: UINavigationController!
    
    var acceptInventoryNavVC: UINavigationController!
    var inventoryQueueNavVC: UINavigationController!
    
    var arr_img:[String] = ["ic_change_password","ic_shareapp","ic_term","ic_feedback","ic_privacy","ic_support","ic_logout2"]
    var arr_name:[String] = ["Change Password","Share App","Terms & Condition","Feedback","Privacy Policy","Support","Logout"]
    
    @IBOutlet weak var tbl_sidemenu: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.mainViewController = self.slideMenuController()?.mainViewController as! UITabBarController!
        self.tbl_sidemenu.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Private Method -
    
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
        case .profile:
            self.slideMenuController()?.changeMainViewController(self.profileNavVC, close: true)
            
        case .acceptInventory:
            self.slideMenuController()?.changeMainViewController(self.acceptInventoryNavVC, close: true)
            
        case .inventoryQueue:
            self.slideMenuController()?.changeMainViewController(self.inventoryQueueNavVC, close: true)
        default:
            break
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        self.slideMenuController()?.closeLeft()
//        if indexPath.row == 0
//        {
//            let contactvc:HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//            self.slideMenuController()?.changeMainViewController(contactvc, close: true)
//        }
//        if indexPath.row == 3
//        {
////             let contactvc:ContactViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
////             self.slideMenuController()?.changeMainViewController(contactvc, close: true)
//        }
     }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_img.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        
        let lbl_name = cell.contentView.viewWithTag(101) as! UILabel
        let img_info = cell.contentView.viewWithTag(102) as! UIImageView
        
        cell.selectionStyle = .none
        lbl_name.text = self.arr_name[indexPath.row]
        img_info.image = UIImage(named: self.arr_img[indexPath.row])  //UIImageView(named: self.arr_img[indexPath.row])
        
        return cell
    }
}
