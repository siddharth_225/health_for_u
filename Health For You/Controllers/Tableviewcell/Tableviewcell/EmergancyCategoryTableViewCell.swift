//
//  EmergancyCategoryTableViewCell.swift
//  Health For You
//
//  Created by SSCS on 08/01/18.
//  Copyright © 2018 SSCS. All rights reserved.
//

import UIKit

class EmergancyCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var img_star1: UIImageView!
    
    @IBOutlet weak var img_star2: UIImageView!
    @IBOutlet weak var img_star3: UIImageView!
    @IBOutlet weak var img_star4: UIImageView!
    @IBOutlet weak var img_star5: UIImageView!
    
    @IBOutlet weak var lbl_availability: UILabel!
    @IBOutlet weak var lbl_Fees: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var img_post: UIImageView!
    @IBOutlet weak var btn_call: UIButton!
    @IBOutlet weak var btn_Book_Now: UIButton!
    
    @IBOutlet weak var lbl_bg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.img_post.layer.cornerRadius = self.img_post.frame.size.height/2
        self.img_post.layer.masksToBounds = true
        
         lbl_bg.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 0, height: 0), radius: 2, scale: true)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
