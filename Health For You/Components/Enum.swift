//
//  Enum.swift
//  longislandloyalty
//
//  Created by  on 10/12/15.
//  Copyright © 2015 aipXperts. All rights reserved.
//

import Foundation
import UIKit

enum ALERT : String {
    case warningTitle	= "Warning!"
    case DefaultTitle	= "Alton!"
    case ErrorTitle		= "Error!"
}

enum Family: String {
	case Bold = "Raleway-Bold"
	case Medium = "Raleway-Medium"
	case Semibold = "Raleway-SemiBold"
	case Regular = "Raleway-Regular"
    
}

enum CoinPackage: String {
	case Coin100 = "com.instaviews.coins.100"
	case Coin1000 = "com.instaviews.coins.1000"
	case Coin3000 = "com.instaviews.coins.3000"
	case Coin8000 = "com.instaviews.coins.8000"
	case Coin25000 = "com.instaviews.coins.25000"
}

extension UIFont {
	class func systemFontOfSize(size: CGFloat) -> UIFont {
		return UIFont(name: Family.Medium.rawValue, size: size)!
	}
    class func systemMediumFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: Family.Regular.rawValue, size: size)!
    }
    class func systemBoldFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: Family.Bold.rawValue, size: size)!
    }
	class func systemLightFontOfSize(size: CGFloat) -> UIFont {
		return UIFont(name: Family.Light.rawValue, size: size)!
	}
}
