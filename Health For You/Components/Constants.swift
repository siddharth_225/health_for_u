//
//  Constants.swift
//  LetsGo
//
//  Created by Ritesh Shah on 21/03/17.
//  Copyright © 2017 Ritesh Shah. All rights reserved.
//

import UIKit

let DEVICE_TYPE : String = "ios"
let DEBUG: Bool = true

let API_URL: String = "http://instaview.aipxperts.com:8080/api/v1/"

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}


let NAV_BG_COLOR = UIColor(red: 32.0 / 255.0, green: 86.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
let APP_BLUE_COLOR = UIColor(red: (36.00/255.0), green: (39.00/255.0), blue: (101.00/255.0), alpha: 1)

let NAV_BORDER_COLOR = UIColor.white
let NAV_TEXT_COLOR = UIColor.white

let ARTICLES_DETAIL:String = "ARTICLES_DETAIL"
