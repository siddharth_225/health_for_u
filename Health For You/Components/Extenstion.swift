//
//  Extenstion.swift
//  iTimePunch
//
//  Created by Hardik on 03/03/15.
//  Copyright (c) 2015 Vivacious. All rights reserved.
//

import UIKit

extension UIButton{
	
	func setCorner(){
		self.layer.cornerRadius = 5.0
		self.layer.masksToBounds = true
	}
}


extension String{
	
	var html2AttributedString: NSAttributedString? {
		guard let data = data(using: .utf8) else { return nil }
		do {
			return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
		} catch let error as NSError {
			print(error.localizedDescription)
			return nil
		}
	}
	var html2String: String {
		return html2AttributedString?.string ?? ""
	}
	
	func heightForWidth(_ width: CGFloat, withFont font: AnyObject) -> CGFloat {
		let attributes = [NSFontAttributeName: font]
		let textStorage = NSTextStorage(string: self, attributes: attributes)
		let textContainer = NSTextContainer(size: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
		let layoutManager = NSLayoutManager()
		layoutManager.addTextContainer(textContainer)
		textStorage.addLayoutManager(layoutManager)
		layoutManager.glyphRange(for: textContainer)
		let height = layoutManager.usedRect(for: textContainer).height
		return ceil(height) + 1
	}
    
    func ConvertDateFormateToAppDateFormate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from:self)!
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dateString = dateFormatter.string(from:date)
        return dateString
    }
}

extension Date{
    
    static func getOnlydate()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy"
		let date = dateFormatter.string(from: Date())
		return date
	}
	
	func getOnlydate()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy"
		let date = dateFormatter.string(from: self)
		return date
	}
	
	static func getOnlyTime()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm"
		let date = dateFormatter.string(from: Date())
		return date
	}
	
	func getOnlyTime()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm"
		let date = dateFormatter.string(from: self)
		return date
	}
	func getOnlyTimeWithDot()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH.mm"
		let date = dateFormatter.string(from: self)
		return date
	}
	func getOnlyTimeWithSeconds()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "hh:mm:ss a"
		let date = dateFormatter.string(from: self)
		return date
	}
	static func getDateFromStringValue(_ str:String)->Date{
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy"
		let date = dateFormatter.date(from: str) ?? Date()
		return date
	}
	static func getDateFromStringWithSeconds(_ dateString:String)->Date{
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss a"
		let date = dateFormatter.date(from: dateString)
		print(date!, terminator: "")
		return date!
	}
	func getOnlydateWithMonthString()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy"
		
		let date = dateFormatter.string(from: self)
		return date
	}
	
	func getDateAndTimeString()->String!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy hh:mm:ss a"
		let date = dateFormatter.string(from: self)
		return date
	}
	
	static func getCurrentTimeStemp()->Int{
		return Int(Date().timeIntervalSince1970)
	}
	
	func getCurrentTimeStemp()->Int{
		return Int(self.timeIntervalSince1970)
	}
	static func getCurrentDateTimeStemp()->Int{
		return getDatebyRemovingTimeFromTimeStamp(Date.getOnlydate())
	}
	
	func getCurrentDateTimeStemp()->Int{
		return Date.getDatebyRemovingTimeFromTimeStamp(self.getOnlydate())
	}
	
	fileprivate static func getDatebyRemovingTimeFromTimeStamp(_ date:String)->Int{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy"
		let date = dateFormatter.date(from: date)
		return Int(date!.timeIntervalSince1970)
	}
	
	static func getTimeStampFromString(_ str:String!)->Int{
		return Int(getDateFromStringValue(str).timeIntervalSince1970)
	}
	static func getStringDateFromTimeStamp(_ timestamp:Int)->String{
		return  Date(timeIntervalSince1970: Double(timestamp)).getOnlydateWithMonthString()
	}
	static func getStringDateAndTimeFromTimeStamp(_ timestamp:Int)->String{
		return  Date(timeIntervalSince1970: Double(timestamp)).getDateAndTimeString()
	}
	static func getStringTimeFromTimeStamp(_ timestamp:Int)->String{
		return  Date(timeIntervalSince1970: Double(timestamp)).getOnlyTimeWithSeconds()
	}
	static func getTimeStampFromStringWithDateandTime(_ timestamp:Int)->String{
		return  Date(timeIntervalSince1970: Double(timestamp)).getOnlyTimeWithSeconds()
	}
	static func getHourMinwithDate(_ time:String!)->Date!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm"
		let date = dateFormatter.date(from: time ?? Date.getOnlyTime()) ?? Date()
		return date
	}
	static func getHourMinwithDateDot(_ time:String!)->Date!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH.mm"
		let date = dateFormatter.date(from: time ?? Date.getOnlyTime()) ?? Date()
		return date
	}
	static func getHourMinSecondswithDate(_ time:String!)->Date!{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "hh:mm:ss a"
		let date = dateFormatter.date(from: time ?? Date.getOnlyTime()) ?? Date()
		return date
	}
	
	func getNextDay()->String{
		return (Calendar.current as NSCalendar).date(
			byAdding: .day,
			value: 1,
			to: self,
			options: NSCalendar.Options(rawValue: 0))!.getOnlydate()
	}
	
	func isGreaterThanDate(_ dateToCompare : Date) -> Bool {
		var isGreater = false
		if self.compare(dateToCompare) == ComparisonResult.orderedDescending
		{
			isGreater = true
		}
		return isGreater
	}
	
}

extension UIView {
// Top Borders
    
    func addTopLeftBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let LeftBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        LeftBottomShapeLayer.frame = CGRect(x: 0, y: 0, width:BorderFrameSize.width/4, height: BorderWidth)
        LeftBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(LeftBottomShapeLayer)
        
        
    }
    
    func addTopRightBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let RightBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        RightBottomShapeLayer.frame = CGRect(x: BorderFrameSize.width - BorderFrameSize.width/4, y: 0, width: BorderFrameSize.width/4, height: BorderWidth)
        RightBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(RightBottomShapeLayer)
    }
    
    func addLeftTopBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let LeftBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        LeftBottomShapeLayer.frame = CGRect(x: 0, y: 0, width: BorderWidth, height: BorderFrameSize.height/4)
        LeftBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(LeftBottomShapeLayer)
        
        
    }
    
    func addRightTopBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let RightBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        RightBottomShapeLayer.frame = CGRect(x: BorderFrameSize.width - BorderWidth, y: 0, width: BorderWidth, height: BorderFrameSize.height/4)
        RightBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(RightBottomShapeLayer)
    }


//Bottom Borders
    
    func addLeftBottomBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let LeftBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        LeftBottomShapeLayer.frame = CGRect(x: 0, y: BorderFrameSize.height - BorderFrameSize.height/4, width: BorderWidth, height: BorderFrameSize.height/4)
        LeftBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(LeftBottomShapeLayer)
        
        
    }
    
    func addRightBottomBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let RightBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        RightBottomShapeLayer.frame = CGRect(x: BorderFrameSize.width - BorderWidth, y: BorderFrameSize.height - BorderFrameSize.height/4, width: BorderWidth, height: BorderFrameSize.height/4)
        RightBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(RightBottomShapeLayer)
    }

    func addBottomLeftBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let LeftBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        LeftBottomShapeLayer.frame = CGRect(x: 0, y: BorderFrameSize.height - BorderWidth, width: BorderFrameSize.width/4, height: BorderWidth)
        LeftBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(LeftBottomShapeLayer)
        
        
    }
    
    func addBottomRightBorder(BorderWidth:CGFloat, BorderColor:CGColor){
        let RightBottomShapeLayer:CALayer = CALayer()
        let BorderFrameSize = self.frame.size
        RightBottomShapeLayer.frame = CGRect(x: BorderFrameSize.width - BorderFrameSize.width/4, y: BorderFrameSize.height - BorderWidth, width: BorderFrameSize.width/4, height: BorderWidth)
        RightBottomShapeLayer.backgroundColor = BorderColor
        self.layer.addSublayer(RightBottomShapeLayer)
    }

    
	func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
		let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
		let mask = CAShapeLayer()
		mask.path = path.cgPath
		self.layer.mask = mask
	}
    
    func setLeftLine()
    {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: 7.0, width: 5.0, height: self.frame.size.height - 14.0)
        border.backgroundColor = NAV_BG_COLOR.cgColor
        self.layer.addSublayer(border)
    }
    
    func setUnderLine(Linewidth:CGFloat){
        
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.size.height - Linewidth, width:self.frame.size.width, height: Linewidth)
        border.backgroundColor = UIColor(red: 215.0/255.0, green: 215.0/255.0, blue: 215.0/255.0, alpha: 1.0).cgColor
        self.layer.addSublayer(border)
    }
//    func addGradientLayer() {
//        
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width , height: self.frame.height) //self.bounds
//        
//        let color1 = BORDER_COLOR.cgColor as CGColor
//        let color2 = NAV_BG_COLOR.cgColor as CGColor
//        
//        gradientLayer.colors = [color1, color2]
//        gradientLayer.locations = [0.0, 1.0]
//        
//        self.layer.addSublayer(gradientLayer)
//    }
    
    func dropShadow(scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0, y: self.frame.size.height - 3.0, width: self.frame.size.width, height: 3.0)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

}

extension UIImage {
	
	func maskWithColor(color: UIColor) -> UIImage? {
		let maskImage = cgImage!
		
		let width = size.width
		let height = size.height
		let bounds = CGRect(x: 0, y: 0, width: width, height: height)
		
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
		let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
		
		context.clip(to: bounds, mask: maskImage)
		context.setFillColor(color.cgColor)
		context.fill(bounds)
		
		if let cgImage = context.makeImage() {
			let coloredImage = UIImage(cgImage: cgImage)
			return coloredImage
		} else {
			return nil
		}
	}
	
}

extension UIViewController {
	
	func setNavigationBarItem() {
		self.addLeftBarButtonWithImage(UIImage(named: "menu")!)
		
		self.slideMenuController()?.removeLeftGestures()
		self.slideMenuController()?.removeRightGestures()
		
		self.slideMenuController()?.addLeftGestures()
	}
	
	func removeNavigationBarItem() {
		self.navigationItem.leftBarButtonItem = nil
		self.navigationItem.rightBarButtonItem = nil
		self.slideMenuController()?.removeLeftGestures()
		self.slideMenuController()?.removeRightGestures()
	}
   
}
