//
//  PurchaseService.swift
//  instaview
//
//  Created by Ritesh Shah on 08/08/17.
//  Copyright © 2017 Ritesh Shah. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class PurchaseService: NSObject {
	
	class var sharedInstance: PurchaseService {
		struct Singleton {
			static let instance = PurchaseService()
		}
		return Singleton.instance
	}
	
	class func purchaseProduct(with identifier: String, completionHandler: responseHandler!) {
		
		SwiftyStoreKit.retrieveProductsInfo([identifier]) { result in
			if let product = result.retrievedProducts.first {
				SwiftyStoreKit.purchaseProduct(product, quantity: 1, atomically: true) { result in
					
					switch result {
					case .success(let purchase):
						completionHandler(purchase.productId, true)
						
					case .error(let error):
						
						var errorString = ""
						switch error.code {
						case .unknown:
							errorString = ("Unknown error. Please contact support")
						case .clientInvalid:
							errorString = ("Not allowed to make the payment")
						case .paymentCancelled:
							break
						case .paymentInvalid:
							errorString = ("The purchase identifier was invalid")
						case .paymentNotAllowed:
							errorString = ("The device is not allowed to make the payment")
						case .storeProductNotAvailable:
							errorString = ("The product is not available in the current storefront")
						case .cloudServicePermissionDenied:
							errorString = ("Access to cloud service information is not allowed")
						case .cloudServiceNetworkConnectionFailed:
							errorString = ("Could not connect to the network")
						case .cloudServiceRevoked:
							errorString = ("User has revoked permission to use this cloud service")
						}
						
						completionHandler(errorString, false)
					}
				}
			} else {
				completionHandler("The product is not available in the current storefront", false)
			}
		}
	}
}
