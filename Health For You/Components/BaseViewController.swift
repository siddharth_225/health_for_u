//
//  BaseViewController.swift
//  fxcoliseum
//
//  Created by  on 24/12/15.
//  Copyright © 2015 aipXperts. All rights reserved.
//

import UIKit
import KVNProgress
import MessageUI
import ReachabilitySwift
import SwiftValidator

class BaseViewController: UIViewController, MFMailComposeViewControllerDelegate {
	
	var strTitle: String = ""
	let validator = Validator()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		validator.styleTransformers(success:{ (validationRule) -> Void in
			
			validationRule.errorLabel?.isHidden = true
			validationRule.errorLabel?.text = ""
			if validationRule.field is UITextField {
                
//				textField.layer.borderColor = UIColor.green.cgColor
//				textField.layer.borderWidth = 0.5
				
			}
		}, error:{ (validationError) -> Void in
			
			validationError.errorLabel?.isHidden = false
			validationError.errorLabel?.text = validationError.errorMessage
			if validationError.field is UITextField {
                validationError.errorLabel?.textColor = NAV_BG_COLOR
//				textField.layer.borderColor = UIColor.red.cgColor
//				textField.layer.borderWidth = 1.0
			}
		})
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if (self.tabBarController != nil)
        {
			self.tabBarController!.tabBar.items?[0].title = "Get Coins"
			self.tabBarController!.tabBar.items?[1].title = "Get Views"
			self.tabBarController!.tabBar.items?[2].title = "Get Likes"
			self.tabBarController!.tabBar.items?[3].title = "Get Followers"
			self.tabBarController!.tabBar.items?[4].title = "Get Comments"
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
        super.viewDidAppear(animated)
		
		if (self.title != nil)
        {
			self.addNavigationTitle(self.title!)
		}
        else if strTitle.characters.count > 0
        {
			self.addNavigationTitle(strTitle)
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		
        super.viewWillDisappear(animated)
		self.view.endEditing(true)
		
		if (self.navigationItem.titleView != nil)
        {
			if self.navigationItem.titleView is UILabel
            {
				let lblTitle = self.navigationItem.titleView as! UILabel
				strTitle = lblTitle.text!
			}
			
			if strTitle != ""
            {
				self.navigationItem.titleView = nil
			}
		}
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	func addNavigationTitle(_ title: String) {
		
		let label = UILabel()
		label.text = title
		label.textColor = NAV_TEXT_COLOR
		label.font = UIFont.systemMediumFontOfSize(size: 20)
		label.adjustsFontSizeToFitWidth = true
		label.sizeToFit()
		
		self.navigationItem.titleView = label
		self.title = nil
	}
	
    func openURL(_ url: URL) {
		
        let webViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webViewController.url = url
        self.navigationController?.pushViewController(webViewController, animated: true)
    }
    
    func isModal() -> Bool {
        
        if self.presentingViewController != nil {
            return true
        }
        if self.presentingViewController?.presentedViewController == self {
            return true
        }
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        return false
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
    
    @IBAction func sendEmailButtonAction(_ sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["support@altonwarehousing.com"])
        mailComposerVC.setSubject("Contact Alton Warehousing Support")
        
        return mailComposerVC
    }
    
    // MARK: - MFMailComposeViewControllerDelegate -
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
	
	// MARK: - Private Method -
	
	func validateField(_ textField: UITextField) {
		
		validator.validateField(textField) { error in
			
		}
	}
}


extension BaseViewController {
	
//	func addBalanceBarItem() {
//		
//		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Balance", style: .done, target: self, action: #selector(balanceBtnAction(sender:)))
//	}
//	
//	@IBAction func balanceBtnAction(sender: AnyObject)
//    {
//		
//		let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "BalanceViewController") as! BalanceViewController
//		self.navigationController?.pushViewController(vc, animated: true)
//	}
}
