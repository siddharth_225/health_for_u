//
//  InstaviewEngine.swift
//  instaview
//
//  Created by Ritesh Shah on 08/08/17.
//  Copyright © 2017 Ritesh Shah. All rights reserved.
//

import UIKit
import InstagramKit

class InstaviewEngine: InstagramEngine {
	
	func postComment(on mediaID: String, comment: String, completionHandler: responseHandler!) {
		
		self.createComment(comment, onMedia: mediaID, withSuccess: { (dict: [AnyHashable : Any]) in
			
		}) { (error: Error, statusCode: Int) in
			completionHandler(error.localizedDescription, false)
		}
	}
	
	func postLike(on mediaID: String, completionHandler: responseHandler!)
    {
		self.likeMedia(mediaID, withSuccess: { (dict: [AnyHashable : Any]) in
			
		}) { (error: Error, statusCode: Int) in
			completionHandler(error.localizedDescription, false)
		}
	}
	
	func followUser(_ userId: String, completionHandler: responseHandler!) {
		
		self.followUser(userId, withSuccess: { (dict: [AnyHashable : Any]) in
			
		}) { (error: Error, statusCode: Int) in
			completionHandler(error.localizedDescription, false)
		}
	}
}
