//
//  WebService.swift
//  LetsGo
//
//  Created by Ritesh Shah on 12/04/17.
//  Copyright © 2017 Ritesh Shah. All rights reserved.
//

import UIKit

class WebService {
	
	class var sharedInstance: WebService {
		struct Singleton {
			static let instance = WebService()
		}
		return Singleton.instance
	}
	
	func processLogin(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!) {
		GlobalData.request(api: "login", dict: dict, method: "POST", completionHandler: completionHandler)
	}
    func Get_InstagramAPI(_ URL: String,completionHandler: responseHandler!) {
        GlobalData.Instarequest(api: URL, dict: [:], method: "GET", completionHandler: completionHandler)
    }
    func RegisterApi(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!)
    {
        GlobalData.request(api: "register", dict: dict, method: "POST", completionHandler: completionHandler)

    }
    func GetAllAccount(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!)
    {
        GlobalData.request(api: "account_list", dict: dict, method: "POST", completionHandler: completionHandler)
    }
    func GetPakage(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!)
    {
        GlobalData.request(api: "packages", dict: dict, method: "POST", completionHandler: completionHandler)
    }
    func GetMediaPost(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!)
    {
        GlobalData.request(api: "media_list", dict: dict, method: "POST", completionHandler: completionHandler)
    }
    func GetCurrentCoins(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!)
    {
        GlobalData.request(api: "get_current_coins", dict: dict, method: "POST", completionHandler: completionHandler)
    }
    func Purchase_request(_ dict: Dictionary<String, Any>,completionHandler: responseHandler!)
    {
        GlobalData.request(api: "purchase_request", dict: dict, method: "POST", completionHandler: completionHandler)
    }
}
