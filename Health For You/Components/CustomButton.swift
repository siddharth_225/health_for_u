//
//  CustomButton.swift
//  alton
//
//  Created by iOS User 1 on 17/07/17.
//  Copyright © 2017 Ritesh Shah. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
	
	override func awakeFromNib() {
		self.setupLayout()
    }
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupLayout()
	}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupLayout()
    }
	
	func setupLayout() {
		
		self.setTitleColor(NAV_TEXT_COLOR, for: .normal)
		self.titleLabel?.font = UIFont.systemFontOfSize(size: 16.0)
		self.backgroundColor = NAV_BG_COLOR
		
		self.layer.cornerRadius = 5
		self.layer.shadowColor = UIColor.black.cgColor
		self.layer.shadowOffset = CGSize(width: 2, height: 2)
		self.layer.shadowOpacity = 0.9
		self.layer.shadowRadius = 3
	}
}
